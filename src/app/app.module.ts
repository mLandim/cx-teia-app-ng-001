import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ListPhotosComponent } from './components/list-photos/list-photos.component';
import { PhotoCardComponent } from './components/photo-card/photo-card.component';
import { PageTitleComponent } from './components/page-title/page-title.component';
import { IconComponent } from './components/icon/icon.component';
import { PopupAlertComponent } from './components/popups/popup-alert/popup-alert.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ListPhotosComponent,
    PhotoCardComponent,
    PageTitleComponent,
    IconComponent,
    PopupAlertComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
