import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListPhotosComponent } from './components/list-photos/list-photos.component';

const routes: Routes = [
  {path: '', component: ListPhotosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
