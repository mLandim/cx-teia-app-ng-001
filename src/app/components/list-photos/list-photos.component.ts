import { Component, OnInit } from '@angular/core';
import { PhotosService} from '../../services/photos.service'
import { Photo } from '../../interfaces/photos.i'

type Order = {key: string, value: string, asc: boolean} 
@Component({
  selector: 'app-list-photos',
  templateUrl: './list-photos.component.html',
  styleUrls: ['./list-photos.component.scss', './list-photos.component-header.scss', './list-photos.component-footer.scss']
})
export class ListPhotosComponent implements OnInit {
  
  public loadingList: boolean
  public photosList: Photo[]
  public searchValue: string
  public itemsByPage: number
  public selectedPage: number

  public orderOptions: Order[] 
  public orderSelected: Order


  constructor(private photosService: PhotosService) {

    this.loadingList = false
    this.photosList = []

    this.searchValue = ''
    this.itemsByPage = 10
    this.selectedPage = 1

    this.orderOptions = [
      {key: 'id', value: 'Id', asc: true},
      {key: 'title', value: 'Título', asc: true},
      {key: 'albumId', value: 'Album', asc: true}
    ]
    this.orderSelected = this.orderOptions[0]

  }

  async ngOnInit(): Promise<void> {
    await this.loadPhotos()
  }

  async loadPhotos() {
    console.log('Loading Photos...')
    this.loadingList = true
    try {
      this.photosList = await this.photosService.loadPhotoList()
    
    } catch (error) {
      console.log(error)
    }
    this.loadingList = false
  }

  deleteItem(id: number) {
    this.photosList = [...this.photosList.filter(photo => photo.id !== id)]
    
  }

  onChangeSelect(e: any) {
    console.log(e.key)
    this.orderSelected = e
  }

  get photosListComputed(): Photo[] {
    let result = this.photosList
    if (this.searchValue.length > 0) {
      result = result.filter(item => JSON.stringify(item).toUpperCase().includes(this.searchValue.toUpperCase()))
    }
    let keyOrder = this.orderSelected.key as keyof Photo
    let ascOrder = this.orderSelected.asc
    result.sort((a, b) => {
      return ascOrder ? a[keyOrder] > b[keyOrder] ? 1 : -1 : a[keyOrder] < b[keyOrder] ? 1 : -1
    })
    return result
  }

  get currentPage(): Photo[] {
    return this.photosListComputed.slice((this.selectedPage - 1) * this.itemsByPage, this.selectedPage * this.itemsByPage)
  }

  get maxPages(): number {
    return Math.ceil(this.photosListComputed.length / this.itemsByPage)
  }

  changePage(i: number) : void {
    this.selectedPage  = this.selectedPage + i
  }

}
