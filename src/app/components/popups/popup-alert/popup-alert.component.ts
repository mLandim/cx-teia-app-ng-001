import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { PopupsService } from '../../../services/popups.service'

@Component({
  selector: 'app-popup-alert',
  templateUrl: './popup-alert.component.html',
  styleUrls: ['./popup-alert.component.scss']
})
export class PopupAlertComponent implements OnInit {
  @Input() id?: string
  private element: any
  public isOpen: boolean = false
  public title: string = ''
  public message: string = ''
  public callbackFunction: CallableFunction | undefined = undefined

  constructor(private popupService: PopupsService, private el: ElementRef) { 

    this.element = el.nativeElement

  }

  ngOnInit(): void {
    this.popupService.register(this)
  }


  open() {
    this.element.style.display = 'flex'
    this.isOpen = true
  }

  close() {
    this.element.style.display = 'none'
    this.isOpen = false
  }

  executeCallback(){
    if (this.callbackFunction) {
      this.callbackFunction()
    }
    this.close()
  }


}
