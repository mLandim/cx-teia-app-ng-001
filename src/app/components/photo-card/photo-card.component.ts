import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Photo } from '../../interfaces/photos.i'

import { PopupsService } from '../../services/popups.service'

@Component({
  selector: 'app-photo-card',
  templateUrl: './photo-card.component.html',
  styleUrls: ['./photo-card.component.scss']
})


export class PhotoCardComponent implements OnInit {
  @Input() photoData: Photo | undefined = undefined
  @Output() deleteEvent = new EventEmitter<number>()

  constructor(private popupService: PopupsService) {

  }

  ngOnInit(): void {
    
  }

  deleteItem(id: number) {
    console.log(`deleteItem`)
    const deleteCallback = () => {
      this.deleteEvent.emit(id)
    }
    this.popupService.showAlert(
      'delete-popup', 
      'Deletar Item', 
      `Confirma a deleção do item com id ${id}?`,
      deleteCallback
      )
  }

  

}
