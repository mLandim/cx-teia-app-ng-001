import { Injectable } from '@angular/core';

import { PopupAlertComponent } from '../components/popups/popup-alert/popup-alert.component'

@Injectable({
  providedIn: 'root'
})
export class PopupsService {
  private popups: PopupAlertComponent[] = []

  constructor() { }

  register(component: PopupAlertComponent) {

    if (!component.id || this.popups.find(c => c.id === component.id)) {
      throw new Error('Componente Popup sem ID ou com ID duplicado')
    }

    this.popups.push(component)

  }

  showAlert(id: string, title?: string, message?: string, callbackFunction?: CallableFunction) {
    const popup = this.popups.find(c => c.id === id)
    if (!popup) {
      throw new Error(`Componente Popup com ID ${id} não existe`)
    }
    popup.title = title ?? ''
    popup.message = message ?? ''
    popup.callbackFunction = callbackFunction ?? undefined
    popup.open()

  }

  closePopup() {
    const popup = this.popups.find(c => c.isOpen)
    popup?.close()

  }

}
