# Desafio Teia :: App Front-end

Este projeto usa a versão 12.2.18 do [Angular CLI](https://github.com/angular/angular-cli) - versão máxima recomendada para uso interno, especialmente considerando a compatibilidade com recursos DSC.

Observando o escopo reduzido do projeto, optou-se por não utilizar bibliotecas/frameworks CSS - concentrando o exercício apenas no Angular e suas capacidades nativas.

Considerando a variedade limitada de componentes necessários, o projeto foi desenvolvido com apenas um módulo - o principal da aplicação.

> Não foram utilizados aqui os componentes DSC - apenas (e em parte) os fundamentos visuais do design system

## Requisitos

- Node (e npm)
- Angular CLI versão 12.x (Para inicialização do projeto, criação de componentes...)

> Preparar o projeto para testes locais: 
>   - Clone este repositório
>   - Execute o comando `npm install` para instalar as dependências do projeto
>   - Por fim, execute o comando `npm run start` pra iniciar o servidor de desenvolviemnto e testar a aplicação localmente  

## Servidor de desenvolvimento

> Devido ao que parece ser uma incompatibilidade da versão do `webpack` utilizada pelo CLI do Angular e as versões mais recentes do Node - tanto no ambiente de desenvolvimento, quanto nos principais ambiente de produção, foi necessário setar uma variável de ambiente

- `NODE_OPTIONS`, com valor `--openssl-legacy-provider`

A variável em questão pode ser vista nos scripts de desenvolvimento local e build

- Para levantar um servidor local e testar a aplicação pode ser usado o comando `npm run start`. A página ficará disponível localmente no endereço `http://localhost:4200/`


## Build

Execute o comando `npm run build` (`ng build`) para preparar o projeto para deploy. Os arquivos serão gerados na pasta `dist/`

## Deploy Vercel

Para este projeto, optamos por realizar o deploy simplificado na plataforma [Vercel](https://vercel.com/).

Usando a interface fornecida pela ferramenta, foi realizada a vinculação deste repositório no gitlab e configurada a variável de ambiente NODE_OPTIONS. Com isso, o sistema da Vercel implementa um pipeline que monitora as atualizações na branch main.

A cada push para o repositório na branch main, a aplicação passa por um processo de build e deploy na plataforma da Vercel.

